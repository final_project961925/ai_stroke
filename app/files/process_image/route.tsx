import { writeFile } from 'fs/promises'
import { NextRequest, NextResponse } from 'next/server'

export async function POST(request: NextRequest) {
  const data = await request.json();
  console.log('file_names',data["file_names"])

  const response = await fetch(`http://localhost:5000/process_image`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ file_list : data["file_names"] }),
  });

  const result = await response.json();
  return NextResponse.json({ success: result['process_image'] })
}