import { NextRequest, NextResponse } from 'next/server'

export async function GET(request: NextRequest) {
  const searchParams = request.nextUrl.searchParams
  const id = searchParams.get('id')
  // const data = await request.json();
  var fileName = id?.split("\\").pop()?.split(".")[0];
  const response = await fetch(`http://localhost:5000/predict`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ file_name: fileName }),

  });
  const result = await response.json();
  return Response.json({ dense: result['dense'],vgg19 : result['vgg19']})
}