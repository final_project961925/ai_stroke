import React from 'react';

const Sidebar = () => {
    return (
        <div
            class="hidden lg:block fixed z-20 inset-0 top-0 left-0 right-auto overflow-y-auto">
            <nav id="nav" class="lg:text-sm lg:leading-6 relative">
                <div class="sticky top-0 -ml-0.5 pointer-events-none">
                    <div class="h-10 bg-white dark:bg-slate-900"></div>
                    <div class="bg-white dark:bg-slate-900 relative pointer-events-auto">
                    </div>
                    <div class="h-8 bg-gradient-to-b from-white dark:from-slate-900"></div>
                </div>
                <ul>
                    <li class="mt-12 lg:mt-8">
                        <h5 class="mb-8 lg:mb-3 font-semibold text-slate-900 dark:text-slate-200">Getting Started</h5>
                        <ul class="space-y-6 lg:space-y-2 border-l border-slate-100 dark:border-slate-800">
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/installation">Installation</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/editor-setup">Editor Setup</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/using-with-preprocessors">Using with Preprocessors</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/optimizing-for-production">Optimizing for Production</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/browser-support">Browser Support</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/upgrade-guide">Upgrade Guide</a></li>
                        </ul>
                    </li>
                    <li class="mt-12 lg:mt-8">
                        <h5 class="mb-8 lg:mb-3 font-semibold text-slate-900 dark:text-slate-200">Core Concepts</h5>
                        <ul class="space-y-6 lg:space-y-2 border-l border-slate-100 dark:border-slate-800">
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/utility-first">Utility-First Fundamentals</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/hover-focus-and-other-states">Hover, Focus, and Other
                                States</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/responsive-design">Responsive Design</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/dark-mode">Dark Mode</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/reusing-styles">Reusing Styles</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/adding-custom-styles">Adding Custom Styles</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/functions-and-directives">Functions &amp; Directives</a></li>
                        </ul>
                    </li>
                    <li class="mt-12 lg:mt-8">
                        <h5 class="mb-8 lg:mb-3 font-semibold text-slate-900 dark:text-slate-200">Customization</h5>
                        <ul class="space-y-6 lg:space-y-2 border-l border-slate-100 dark:border-slate-800">
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/configuration">Configuration</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/content-configuration">Content</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/theme">Theme</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/screens">Screens</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/customizing-colors">Colors</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/customizing-spacing">Spacing</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/plugins">Plugins</a></li>
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/presets">Presets</a></li>
                        </ul>
                    </li>
                    <li class="mt-12 lg:mt-8">
                        <h5 class="mb-8 lg:mb-3 font-semibold text-slate-900 dark:text-slate-200">Base Styles</h5>
                        <ul class="space-y-6 lg:space-y-2 border-l border-slate-100 dark:border-slate-800">
                            <li><a
                                class="block border-l pl-4 -ml-px border-transparent hover:border-slate-400 dark:hover:border-slate-500 text-slate-700 hover:text-slate-900 dark:text-slate-400 dark:hover:text-slate-300"
                                href="https://tailwindcss.com/docs/preflight">Preflight</a></li>
                        </ul>
                    </li>
                </ul>
            </nav >
        </div >
    );
};

export default Sidebar;
{/* <div className="relative items-center justify-center w-[248px] overflow-y-auto fixed top-0 left-0 bg-gray-200">
                <div className="sticky top-0 bg-black">
                    <div className="pt-[4px] text-center [font-family:'Inter-Regular',Helvetica] font-normal text-[#a2dadd] text-[14.8px]"> Studies </div>
                    <div className="relative flex flex-col items-center justify-center pb-[17px]  border-b-[1px] [border-bottom-style:solid] border-[#087d88]">
                        <div className="inline-flex pt-[5px]">
                            <button className="relative p-[8px] border-solid rounded-[6px_0px_0px_6px] border-y-[0.8px] border-l-[0.8px] border-[#087d88] [font-family:'Inter-Regular',Helvetica] font-normal text-white text-[13px] text-center ">Primary</button>
                            <button className="relative p-[8px] border-solid border-y-[0.8px] border-[#087d88] [font-family:'Inter-Regular',Helvetica] font-normal text-white text-[13px] text-center ">Recent</button>
                            <button className="relative p-[8px] border-solid rounded-[0px_6px_6px_0px] border-y-[0.8px] border-[#087d88] border-r-[0.8px] [font-family:'Inter-Regular',Helvetica] font-normal text-white text-[13px] text-center ">All</button>
                        </div>
                    </div>
                </div>
                <div className="mb-4 p-4 bg-white rounded">Item 1</div>
                <div className="mb-4 p-4 bg-white rounded">Item 2</div>
                <div className="mb-4 p-4 bg-white rounded">Item 3</div>
                <div className="mb-4 p-4 bg-white rounded">Item 1</div>
                <div className="mb-4 p-4 bg-white rounded">Item 2</div>
                <div className="mb-4 p-4 bg-white rounded">Item 3</div>
                <div className="mb-4 p-4 bg-white rounded">Item 1</div>
                <div className="mb-4 p-4 bg-white rounded">Item 2</div>
                <div className="mb-4 p-4 bg-white rounded">Item 3</div>
                <div className="mb-4 p-4 bg-white rounded">Item 1</div>
                <div className="mb-4 p-4 bg-white rounded">Item 2</div>
                <div className="mb-4 p-4 bg-white rounded">Item 3</div>
            </div> */}