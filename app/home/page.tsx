import React from 'react'
import ToolBar from '../components/ToolBar'
import Sidebar from '../components/sidebar'
import MainDisplay from '../components/MainDisplay'
import UtilizationBar from '../components/UtilizationBar'

const page = () => {

    return (
        <div className='min-h-screen flex flex-col '>
            <div className='pb-[5px]'><ToolBar /></div>
            <div className='relative flex flex-grow'>
                <div className='flex left-[-20px]'><Sidebar /></div>
                <MainDisplay />
                <div className='pl-[5px]'><UtilizationBar /></div>
            </div>
        </div>
    )
}

export default page