import React from 'react'

const Test = () => {
    return (
        <div className="min-h-screen flex flex-col">
            <div className=" px-2 flex items-center justify-between border-b-[1px] border-gray-700 bg-[#00293b] ">
                <div className="items-center flex p-1">
                    <button>
                        <img className="relative w-[30px] h-[30px]" alt="=Back Button" src="Back.svg" />
                    </button>
                </div>
            </div>
            <div className='flex flex-grow'>
                <nav className='bg-white text-center shadow-sm p-6 space-y-6 w-64'> Navbar </nav>
                <main className='bg-gray-100 flex-1 p-6'>
                    <div class="flex justify-center items-center h-screen">
                        <div class="w-64 h-64 bg-blue-500">
                            Content will go here
                        </div>
                    </div>
                </main>

            </div>

            <div className="flex justify-between">
                <div className="bg-blue-500 w-24 h-24"></div>
                <div className="bg-red-500 w-24 h-24"></div>
                <div className="bg-green-500 w-24 h-24"></div>
            </div>
        </div>
    )
}

export default Test