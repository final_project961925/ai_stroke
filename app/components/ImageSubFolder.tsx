'use client'
import React, { useState } from 'react'
import ImageContainer from './ImageContainer'

type ChildComponentProps = { subdir : string[]; images: string[] };

const ImageSubFolder = ({ subdir , images}: any) => {
    const [showContent, setShowContent] = useState(true);

    const toggleContent = () => {
        setShowContent(!showContent);
    };

    return (
        <div>
            <button className={`group relative ${showContent ? 'hover:bg-black' : 'bg-black hover:bg-inherit'}  pt-[5px] pb-[5px] px-[16px] border-b-[1px] [border-bottom-style:solid] border-[#087d88] w-[248px] overflow-hidden`} onClick={toggleContent}>
                <div className="flex content-center justify-between ">
                    <div className="pt-[5px] [font-family:'Inter-Regular',Helvetica] font-bold text-[#087d88] text-[16px] group-hover:text-white">{subdir}</div>
                    <div className="flex align-middle">
                    </div>
                </div>
            </button>
            {showContent ? null : images.map((image: any) => {
                return <li key={image.id} className='text-white'><ImageContainer image={image}/></li>
            })}
        </div>

    )
}

export default ImageSubFolder