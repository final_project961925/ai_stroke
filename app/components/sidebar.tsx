"use server"
import React from 'react'
import { glob } from 'glob';
import ImageFolder from './ImageFolder';

const Sidebar =  () => {
    const fs = require('fs');
    const path = require('path');

    const getDirectories = (source: any) =>
        fs.readdirSync(source, { withFileTypes: true })
            .filter((dirent : any) => dirent.isDirectory())
            .map((dirent : any) => dirent.name);

    function getSubdirectories(directoryPath : any) {
        try {
            // Get an array of all files and directories in the specified directory
            const items = fs.readdirSync(`public\\stroke\\${directoryPath}`);
            return items;
        } catch (err) {
            console.error(`Error reading directory: ${err}`);
            return [];
        }
    }

    const directories = getDirectories(path.join("./public/stroke"))

    return (
        <div className='relative items-center justify-center w-[248px]'>
            <div
                className="absolute flex hidden lg:block fixed z-20 inset-0 top-0 left-0 right-auto overflow-y-auto w-[248px]">
                <nav id="nav" className="lg:text-sm relative">
                    <div className=" bg-black">
                        <div className="pt-[4px] text-center [font-family:'Inter-Regular',Helvetica] font-normal text-[#a2dadd] text-[14.8px] pb-[17px]  border-b-[1px] border-[#087d88]"> Studies </div>
                    </div>
                    <div className='relative overflow-hidden'>
                        <ul>
                            {directories.length
                                ? directories.map( async (dir: any) => {
                                    const images =  await glob(`public/stroke/${dir}/*/*.{png,jpg}`);
                                    const subdir = getSubdirectories(dir)
                                    return <li key={dir.id}><ImageFolder date={dir} subdir={subdir} images={images} /></li>
                                })
                                : null}
                        </ul>
                    </div>
                </nav >
            </div >
        </div>
    )
}

export default Sidebar