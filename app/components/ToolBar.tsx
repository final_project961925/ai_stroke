'use client'
import React from 'react'
import { useRouter } from 'next/navigation';
import { useToolBarContext } from '@/app/Contexts/Toolbar-Contexts';
import html2canvas from 'html2canvas';

const ToolBar = () => {
    const router = useRouter()
    const {tool, setTool} = useToolBarContext()
    const captureAndSaveScreenshot = () => {
        // Capture the entire viewport
        html2canvas(document.body, { backgroundColor: "#000000" }).then((canvas) => {
            const screenshotDataUrl = canvas.toDataURL('image/png');

            // Create a link element to download the screenshot
            const a = document.createElement('a');
            a.href = screenshotDataUrl;
            a.download = 'screenshot.png'; // Set the desired file name

            // Trigger a click event to initiate the download
            a.click();
        });
    };
    return (
        <div className=" px-2 flex-1 items-center justify-between  bg-[#00293b] ">
            <div className="items-center flex p-1">
                <button onClick={() => router.push('/', { scroll: false })}>
                    <img className="relative w-[30px] h-[30px]" alt="Back Button" src="Back.svg" />

                </button>
                {/* <Imagemagnifyer isVisible={isMagnifierVisible} /> */}
                <div className="items-center flex p-1 mx-auto" >
                    {/* <button className='border border-white p-2 rounded' onClick={()=>{console.log("test"); setMagnifierVisibility(!isMagnifierVisible); setLineVisibility(false);}}> */}
                    <button className='border border-white p-2 rounded' onClick={() => { console.log("test"); setTool('Glass')}}>
                        <img className="relative w-[30px] h-[25px]" alt="=Glass Button" src="Glass.svg" />
                    </button>
                    {/* <button className='ml-6 border border-white p-2 rounded' onClick={()=>{console.log("testline"); setLineVisibility(!isLineVisible); setMagnifierVisibility(false);}}> */}
                    <button className='ml-6 border border-white p-2 rounded' onClick={() => { console.log("testline"); setTool('Ruler')}}>
                        <img className="relative w-[30px] h-[25px]" alt="=Line Button" src="Ruler.svg" />
                    </button>
                    <button onClick={captureAndSaveScreenshot} className='ml-6 border border-white p-2 rounded'>
                        <img className="relative w-[30px] h-[25px]" alt="=Camera Button" src="Camera.svg" />
                    </button>
                </div>
            </div>
        </div>
    )
}

export default ToolBar