"use client"
import React, { useRef, useEffect, useState,useContext } from 'react';
import { useToolBarContext } from '@/app/Contexts/Toolbar-Contexts';
import InfoBar from './InfoBar';
import Image from 'next/image';
import ReactImageMagnify from '@blacklab/react-image-magnify';
import { useImageContext } from '../Contexts/Image-Contexts'

// const MainDisplay = ({ isMagnifierVisible, isLineVisible}: MainProps) => {
const MainDisplay = () => {
  const {imagePath} = useImageContext()
  const {tool} = useToolBarContext();
  const imageDpi = 60;
  const canvasRef = useRef<HTMLCanvasElement | null>(null);
  const [isDrawing, setIsDrawing] = useState(false);
  const [startPosition, setStartPosition] = useState({ x: 0, y: 0 });
  const [stopPosition, setStopPosition] = useState({ x: 0, y: 0 });
  const [canvasVisible, setCanvasVisible] = useState(false);
  const [lengthofline, setlineoflength] = useState(0);

  const drawStraightLine = (startX: number, startY: number, stopX: number, stopY: number, ctx: CanvasRenderingContext2D | null) => {
    if (!ctx) return;

    ctx.clearRect(0, 0, canvasRef.current!.width, canvasRef.current!.height);
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineTo(stopX, stopY);
    ctx.strokeStyle = 'red';
    ctx.closePath();
    ctx.stroke();
  };

  useEffect(() => {
    const canvas = canvasRef.current;
    if (!canvas) return;

    const ctx = canvas.getContext('2d');

    const handleMouseDown = (event: MouseEvent) => {
      setIsDrawing(true);
      const rect = canvas.getBoundingClientRect();
      setStartPosition({ x: event.clientX - rect.left, y: event.clientY - rect.top });
    };

    const handleMouseMove = (event: MouseEvent) => {
      if (!isDrawing) return;

      const rect = canvas.getBoundingClientRect();
      const currentX = event.clientX - rect.left;
      const currentY = event.clientY - rect.top;

      drawStraightLine(startPosition.x, startPosition.y, currentX, currentY, ctx);
      setStopPosition({
        x: currentX,
        y: currentY,
      });
    };

    const handleMouseUp = () => {
      setIsDrawing(false);

      // Calculate length only when drawing is complete
      const pixelLength = Math.sqrt(Math.pow(startPosition.x - stopPosition.x, 2) + Math.pow(startPosition.y - stopPosition.y, 2));
      const physicalLength = pixelLength / imageDpi;
      console.log(`line length = ${physicalLength} inches (image at ${imageDpi} dpi)`);
      setlineoflength(physicalLength);
    };

    canvas.addEventListener('mousedown', handleMouseDown);
    canvas.addEventListener('mousemove', handleMouseMove);
    canvas.addEventListener('mouseup', handleMouseUp);

    return () => {
      canvas.removeEventListener('mousedown', handleMouseDown);
      canvas.removeEventListener('mousemove', handleMouseMove);
      canvas.removeEventListener('mouseup', handleMouseUp);
    };
  }, [isDrawing, startPosition, stopPosition]);


  const isMagnifierVisible = () => {
    return (tool === "Glass")
  }

  const isLineVisible = () => {
    return (tool === "Ruler")
  }

  return (
    <div className="relative flex w-full flex-col pl-[5px] rounded-[6px] text-white border border-solid border-[#a2dadd] ">
      <InfoBar />
      <div className={`relative flex h-full p-[5px] items-center  ${isMagnifierVisible() ? 'flex' : 'hidden'}`}>
        <div className="relative flex h-full aspect-square ">
          <div id="imagemagnifyer">
            <ReactImageMagnify
              activationInteractionHint="click"
              imageProps={{
                alt: 'Wristwatch by Ted Baker London',
                src: '/image.png',
                width: '90%',
                height: '90%',
              }}
              magnifiedImageProps={{
                src: '/image.png',
                width: 433 *0.9,
                height: 542 *1.8,
      
              }}
              onActivationChanged={function noRefCheck(){}}
              onDetectedEnvironmentChanged={function noRefCheck(){}}
              onPositionChanged={function noRefCheck(){}}
              // portalProps={{
              //   id: 'portal-test-id'
              // }}
            />
          </div>
        </div>
      </div>
      <div className={`relative h-full p-[5px] justify-center items-center ${isLineVisible()  ? 'flex' : 'hidden'}`}>
        <div className="relative flex h-full aspect-square overflow-hidden">
          {/* Image goes here */}
          <Image
            src={imagePath}
            height={450}
            width={450}
            alt="ruler Display Image"
            className='select-none'
          />
          {/* Canvas overlay */}
          <canvas id="canvas" ref={canvasRef} width={450} height={450} className="absolute top-0 left-0" />
          {/* Line length information */}
            <div className="absolute top-0 left-0 text-red-500 ">
              {`Line length = ${lengthofline.toFixed(2)} inches`}
            </div>
        </div>
      </div>
      <div className={`relative h-full p-[5px] justify-center items-center  ${isMagnifierVisible() || isLineVisible() ? 'hidden' : 'flex'}`}>
        <div className="relative flex h-full aspect-square overflow-hidden " >
          <Image
            src={imagePath} 
            height={450} 
            width={450} 
            alt="Main Display Image" />
        </div>
      </div>
    </div>
  );
};

export default MainDisplay;
