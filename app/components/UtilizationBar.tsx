'use client'
import React from 'react'
import Image from 'next/image'
import { useSearchParams } from 'next/navigation'
import { useImageContext } from '../Contexts/Image-Contexts'

const utilBar = () => {

    const {ID , predictions} = useImageContext()
    const searchParams = useSearchParams()

    // blood area 
    let bloodarea = searchParams.get('blood')!
    if(!bloodarea){
      bloodarea = "Defalut"
    }
    // length of lines
    let lengthofline = searchParams.get('lengthofline')!
    if(!lengthofline){
      lengthofline = "Defalut"
    }
    async function predict(files: any) {
        if (!files) return
    
        function GetFileName(file:any) {
          return file["name"]
        }
    
        const file_list = files.map(GetFileName)
        const obj = {file_names:file_list}
        try {
    
          const res = await fetch('/files/process_image', {
            method: 'POST',
            body: JSON.stringify(obj)
          })
          // handle the error
          if (!res.ok) throw new Error(await res.text())
        } catch (e: any) {
          // Handle errors here
          console.error(e)
        }
      }

    return (
        <div className="items-center justify-center text-center w-[248px] my-4">
          <button className="relative p-[8px]  [font-family:'Inter-Regular',Helvetica] font-normal text-[#a2dadd] text-[15.8px] text-center ">Report</button>
          <div className="flex-col items-start">
              <div className=" flex-1  pt-[5px] pb-[5px] px-[16px] my-2 border-b-[1px] [border-bottom-style:solid] border-[#087d88]">
                  <div className="flex content-center justify-between ">
                      <div className="[font-family:'Inter-Regular',Helvetica] font-normal text-[19.8px] text-white my-2">Patient's ID: {ID}</div>
                  </div>
              </div>
              <div className=" flex-1  pt-[5px] pb-[5px] px-[16px] my-2 border-b-[1px] [border-bottom-style:solid]  border-[#087d88]">
                  <div className="flex content-center justify-between ">
                      <div className="[font-family:'Inter-Regular',Helvetica] font-normal text-[19.8px] text-white my-2">Stroke type: {predictions.join(',')}</div>
                  </div>
              </div>
              <div className=" flex-1  pt-[5px] pb-[5px] px-[16px] my-2 border-b-[1px] [border-bottom-style:solid] border-[#087d88]">
                  <div className="flex content-center justify-between ">
                      <div className="[font-family:'Inter-Regular',Helvetica] font-normal text-[19.8px] text-white my-2">Blood Volume: {bloodarea}</div>
                  </div>
              </div>
              <div className=" flex-1  pt-[5px] pb-[5px] px-[16px] my-2 border-b-[1px] [border-bottom-style:solid] border-[#087d88]">
                  <div className="flex content-center justify-between ">
                      <div className="[font-family:'Inter-Regular',Helvetica] font-normal text-[19.8px] text-white my-2">Line length : {lengthofline}</div>
                  </div>
              </div>
          </div>          
    </div>
    )
}

export default utilBar  