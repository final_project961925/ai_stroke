import React from 'react'
import Image from 'next/image';

type Texts = {
    texts : string;
}


const ArrowButton = (texts: Texts, event : Event) => { 
    return (
        <div className="inline-flex items-start relative">
            <div className="inline-flex gap-[12px] p-[12px] relative flex-[0_0_auto] bg-[#a2dadd] hover:bg-[#98cdcf] active:bg-[#8abdbe] rounded-[15000px] box-border">
                <div className="relative [font-family:'Inter-Medium',Helvetica] font-medium text-black text-[24px] text-center">
                    {texts.texts}
                </div>
                <Image width={24} height={24} alt="Arrow Icon" src="arrow.svg" className='pt-[5px]'/>
            </div>
        </div>
    )
}

export default ArrowButton