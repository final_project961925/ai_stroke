'use client'
import React, { useState } from 'react'
import ImageContainer from './ImageContainer'
import ImageSubFolder from './ImageSubFolder'

type ChildComponentProps = { date: string;subdir : string[]; images: string[] };

const ImageFolder = ({ date,subdir, images }: ChildComponentProps) => {
    const [showContent, setShowContent] = useState(true);
    const filenumber = '10'

    const toggleContent = () => {
        setShowContent(!showContent);
    };

    const openedContent = (
        <div className="flex pt-1 [font-family:'Inter-Regular',Helvetica] font-normal text-[#90cdf4] text-[20px] tracking-[0] leading-[30px] whitespace-nowrap">CT
        </div>
    );
    return (
        <div>
            <button className={`relative ${showContent ? 'hover:bg-[#087d88]' : 'bg-[#087d88] hover:bg-inherit'}  pt-[5px] pb-[5px] px-[16px] border-b-[1px] [border-bottom-style:solid] border-[#087d88] w-[248px] overflow-hidden`} onClick={toggleContent}>
                <div className="flex content-center justify-between ">
                    <div className="pt-[5px] [font-family:'Inter-Regular',Helvetica] font-bold text-white text-[16px]">{date}</div>
                    <div className="flex align-middle">
                        <img alt="Folder Icon" src="folder-icon.svg" />
                        <div className="pl-1 pt-1 [font-family:'Inter-Regular',Helvetica] font-normal text-[#90cdf4] text-[13px]">
                            {filenumber}
                        </div>
                    </div>
                </div>
                {showContent ? openedContent : null}
            </button>
            {showContent ? null : subdir.map((dir: any) => {
                return <li key={dir.id} className='text-white'><ImageSubFolder subdir={dir} images={images}/></li>
            })}
        </div>

    )
}

export default ImageFolder