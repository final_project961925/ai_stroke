import React from 'react'

const DetailBar = () => {
  return (
    <div className="relative w-[248px] h-[1028px] bg-white">
      <div className="flex w-[248px] h-[36px] items-start px-[10px] py-0 absolute top-0 left-0 bg-">
        <button className="relative flex-1 min-w-[96px] grow h-[36px] all-[unset] box-border">
          <div className="absolute h-[15px] top-[9px] left-[88px] [font-family:'Inter-Regular',Helvetica] font-normal text-[#a2dadd] text-[14.8px] text-center tracking-[0] leading-[14.8px] whitespace-nowrap">
            Studies
          </div>
          <img className="absolute w-[20px] h-[12px] top-[12px] left-[203px]" alt="Svg" src="SVG.svg" />
        </button>
      </div>
    </div>
  )
}

export default DetailBar