'use client'
import Image from 'next/image';
import React, { useState } from 'react'
import { useRouter } from 'next/navigation'
import { useImageContext } from '../Contexts/Image-Contexts';

const ImageContainer = ({ image }: any) => {
    const router = useRouter()
    const { setID, setPredictions , setImagePath } = useImageContext()

    image = image.replace("public", "").replace("\\", '/')
    const result = image.split("?")

    const toggleContent = async () => {
        try {
            const res = await fetch(`/files/predict?id=${image}`, {
                method: 'GET'
            })
            const data = await res.json()
            if (!res.ok) throw new Error(await res.text())
            setImagePath(image)
            setID(image.split('\\').find((part: string) => part.startsWith('ID_')))
            const tempArray : string[] = [data['dense'], data['vgg19']]
            setPredictions(tempArray)
        } catch (e: any) {
            // Handle errors here
            console.error(e)
        }

    }

    return (
        <button className=" flex items-start flex-row py-5 " onClick={toggleContent}>
            <img className={`relative pr-[10px]`} alt="Circle Icon" src="circle.svg" />
            <div className='relative flex overflow-hidden p-[3px] rounded-[6px] border border-solid border-[#a2dadd]'>
                <Image
                    src={image}
                    height={150}
                    width={150}
                    alt='picture mode'
                />
            </div>
        </button>
    )
}

export default ImageContainer