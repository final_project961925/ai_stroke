"use client";
import React, { createContext, useContext, useState } from "react";

type ImageContext = {
    ID: string;
    setID: React.Dispatch<React.SetStateAction<string>>;
    imagePath : string;
    setImagePath : React.Dispatch<React.SetStateAction<string>>;
    predictions: string[];
    setPredictions: React.Dispatch<React.SetStateAction<string[]>>;
}

const ImageContext = createContext<ImageContext | null>(null);

export function ImageProvider({ children }: { children: React.ReactNode }) {
    const [ID, setID] = useState('ID')
    const [predictions ,setPredictions] = useState<string[]>([])
    const [imagePath, setImagePath] = useState('/image.png')
    return (
        <ImageContext.Provider value={{ ID, setID , predictions, setPredictions , imagePath, setImagePath }}>
            {children}
        </ImageContext.Provider>

    );
};

export function useImageContext() {
    const context = useContext(ImageContext);
    if (!context) {
        throw new Error(
            "UseThemeContext must be used within a ThemeContextProvider"
        )
    }
    return context
};