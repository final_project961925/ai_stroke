"use client";
import React, { createContext, useContext, useState } from "react";

type ToolbarContext = {
    tool: string;
    setTool: React.Dispatch<React.SetStateAction<string>>;
}

const ToolbarContext = createContext<ToolbarContext | null>(null);

export function ToolBarProvider({ children }: { children: React.ReactNode }) {
    const [tool, setTool] = useState('tools')
    return (
        <ToolbarContext.Provider value={{ tool, setTool }}>
            {children}
        </ToolbarContext.Provider>

    );
};

export function useToolBarContext() {
    const context = useContext(ToolbarContext);
    if (!context) {
        throw new Error(
            "UseThemeContext must be used within a ThemeContextProvider"
        )
    }
    return context
};