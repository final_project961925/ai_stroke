"use client";

import { useRef, useState } from "react";
import { useRouter } from 'next/navigation'

const UploadPage = () => {
  const router = useRouter()
  const [dragActive, setDragActive] = useState<boolean>(false);
  const inputRef = useRef<any>(null);
  const [files, setFiles] = useState<any>([]);

  function handleChange(e: any) {
    e.preventDefault();
    console.log("File has been added");
    if (e.target.files && e.target.files[0]) {
      console.log(e.target.files);
      for (let i = 0; i < e.target.files["length"]; i++) {
        setFiles((prevState: any) => [...prevState, e.target.files[i]]);
      }
    }
  }

  async function upload(file: any) {
    if (!file) return

    try {
      const data = new FormData()
      data.set('file', file)

      const res = await fetch('/files/upload', {
        method: 'POST',
        body: data
      })
      // handle the error
      if (!res.ok) throw new Error(await res.text())
    } catch (e: any) {
      // Handle errors here
      console.error(e)
    }
  }

  async function process_image(files: any) {
    if (!files) return

    function GetFileName(file:any) {
      return file["name"]
    }

    const file_list = files.map(GetFileName)
    const obj = {file_names:file_list}
    try {

      const res = await fetch('/files/process_image', {
        method: 'POST',
        body: JSON.stringify(obj)
      })
      // handle the error
      if (!res.ok) throw new Error(await res.text())
    } catch (e: any) {
      // Handle errors here
      console.error(e)
    }
  }

  async function handleSubmitFile(e: any) {
    if (files.length === 0) {
      throw new Error('No file uploaded')
    } else {
      console.log(files)
      files.map(upload)
      process_image(files)
      router.push('/home', { scroll: false })
    }
  }

  function handleDrop(e: any) {
    e.preventDefault();
    e.stopPropagation();
    setDragActive(false);
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      for (let i = 0; i < e.dataTransfer.files["length"]; i++) {
        setFiles((prevState: any) => [...prevState, e.dataTransfer.files[i]]);
      }
    }
  }

  function handleDragLeave(e: any) {
    e.preventDefault();
    e.stopPropagation();
    setDragActive(false);
  }

  function handleDragOver(e: any) {
    e.preventDefault();
    e.stopPropagation();
    setDragActive(true);
  }

  function handleDragEnter(e: any) {
    e.preventDefault();
    e.stopPropagation();
    setDragActive(true);
  }

  function removeFile(fileName: any, idx: any) {
    const newArr = [...files];
    newArr.splice(idx, 1);
    setFiles([]);
    setFiles(newArr);
  }

  function openFileExplorer() {
    inputRef.current.value = "";
    inputRef.current.click();
  }

  return (
    <div className="flex items-center justify-center absolute  inset-0">
      {/* border-[5px] border-solid border-[#a2dadd] */}
      <div className="flex-col p-[25px] bg-[#8ad9dd] relative rounded-[20px] ">
        {/* <form className="flex justify-around relative" action={upload}>
          <input type="file" name="file" id='load' hidden />
          <label htmlFor='load'>
            <ArrowButton texts='Browse' />
          </label>
          <input type="submit" id='submit' hidden />
          <label htmlFor='submit'>
            <ArrowButton texts='Submit' />
          </label>
        </form> */}
        <form
          className={`${dragActive ? "bg-[#8ad9dd]" : "bg-[#8ad9dd]"
            }  p-4 rounded-lg border-[5px] border-dashed border-[#000000] min-h-[10rem] text-center flex flex-col items-center justify-center`}
          onDragEnter={handleDragEnter}
          onSubmit={(e) => e.preventDefault()}
          onDrop={handleDrop}
          onDragLeave={handleDragLeave}
          onDragOver={handleDragOver}
        >
          <input
            placeholder="fileInput"
            className="hidden"
            ref={inputRef}
            type="file"
            multiple={true}
            onChange={handleChange}
            accept=".dcm"
          />

          <p className="[font-family: 'Nova Square', sans-serif;] font-semibold text-black text-[54px] text-center">
            Drag DICOM files here,or{" "}
            <span
              className="font-bold  text-[#1f3031] cursor-pointer"
              onClick={openFileExplorer}
            >
              <u> Browse</u>
            </span>{" "}
          </p>

          <div className="flex flex-col items-center p-3">
            {files.map((file: any, idx: any) => (
              <div key={idx} className="flex flex-row space-x-5">
                <span>{file.name}</span>
                <span
                  className="text-red-500 cursor-pointer"
                  onClick={() => removeFile(file.name, idx)}
                >
                  remove
                </span>
              </div>
            ))}
          </div>

          <button
            className="bg-black rounded-lg p-2 mt-3 w-auto"
            onClick={handleSubmitFile}
          >
            <span className="p-2 text-white">Submit</span>
          </button>
        </form>
      </div>
    </div>
  )
}

export default UploadPage
